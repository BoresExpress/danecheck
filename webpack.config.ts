import webpack from 'webpack'
import path from 'path'
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'

const webpackConfig: webpack.Configuration = {
	context: __dirname,
	entry: {
		background: './src/background/background.ts',
	},
	output: {
		path: path.resolve('dist'),
		filename: '[name].js',
	},
	devtool: 'inline-source-map',
	resolve: {
		extensions: ['.ts'],
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				exclude: /node_modules/,
				loader: 'ts-loader',
				options: {
					transpileOnly: true,
				},
			},
		],
	},
	plugins: [
		new ForkTsCheckerWebpackPlugin({ eslint: true }),
		new CopyWebpackPlugin([
			{ from: './src/manifest.json', to: './' },
			{ from: './src/assets/icon/*.png', to: './assets/icon', flatten: true },
		]),
	],
}

export default webpackConfig
