export type Returns<Result> = Promise<Result | undefined>

type Method<Result> = (url: string, data?: any, headers?: HeadersInit) => Returns<Result>

export default interface IHttpClient<Result> {
	get: Method<Result>
	put: Method<Result>
	post: Method<Result>
	delete: Method<Result>
	abort: () => void
}
