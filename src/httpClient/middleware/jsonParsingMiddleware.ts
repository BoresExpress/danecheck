import { Middleware } from '../HttpClient'
import { Returns } from '../IHttpClient'

export default function<Result>(contentTypes?: string[]): Middleware<Result> {
	return async(response: Response): Returns<Result> => {
		const contentTypeHeaders = response.headers.get('content-type')
		if (contentTypeHeaders) {
			const types = contentTypes ? contentTypes : ['application/json']
			for (const type of types) {
				if (contentTypeHeaders.includes(type)) {
					const text = await response.text()
					if (text.length > 0) {
						return JSON.parse(text)
					}

					break
				}
			}
		}

		return void 0
	}
}
