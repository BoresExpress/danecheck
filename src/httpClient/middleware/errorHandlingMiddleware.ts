import HttpError from '../HttpError'
import { Returns } from '../IHttpClient'

export default function<Result>(response: Response, prevResult?: Result): Returns<Result> {
	if (!response.ok) {
		throw new HttpError(response.status, response.statusText, response.url, prevResult)
	}

	return Promise.resolve(prevResult)
}
