import IHttpClient, { Returns } from './IHttpClient'

export type Middleware<Result> = (
	r: Response,
	prevResult?: Result,
) => Returns<Result>

interface Options<Result> {
	host?: string
	fetchOptions?: RequestInit
	middlewares?: Middleware<Result>[]
	preventCaching?: boolean
}

export default class HttpClient<Result> implements IHttpClient<Result> {
	private static readonly bodyMethods = new Set(['POST', 'PUT', 'DELETE'])

	private readonly host: string
	private controller?: AbortController
	private readonly fetchOptions: RequestInit
	private readonly middlewares: Middleware<Result>[]
	private readonly preventCaching: boolean

	constructor(options: Options<Result> = {}) {
		const { fetchOptions = {}, middlewares = [], host = '', preventCaching = false } = options

		this.host = host
		this.fetchOptions = fetchOptions
		this.middlewares = middlewares
		this.preventCaching = preventCaching
	}

	public get(url: string, data?: any, headers?: HeadersInit): Returns<Result> {
		return this.request('GET', url, data, headers)
	}
	public put(url: string, data?: any, headers?: HeadersInit): Returns<Result> {
		return this.request('PUT', url, data, headers)
	}
	public post(url: string, data?: any, headers?: HeadersInit): Returns<Result> {
		return this.request('POST', url, data, headers)
	}
	public delete(url: string, data?: any, headers?: HeadersInit): Returns<Result> {
		return this.request('DELETE', url, data, headers)
	}
	public abort(): void {
		if (this.controller) {
			this.controller.abort()
		}
	}

	private async request(method: string, url: string, data: any = null, headers: HeadersInit = {}): Returns<Result> {
		this.controller = new AbortController()

		const { headers: optsHeaders = {}, ...opts } = this.fetchOptions

		const withBody = HttpClient.bodyMethods.has(method)

		url = withBody ? url : `${url}${this.toQueryString(data)}`
		url = this.host ? `${this.host}/${url}` : url

		const response = await fetch(url, {
			method,
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json; charset=utf-8',
				...optsHeaders,
				...headers,
			},
			...opts,
			body: withBody ? JSON.stringify(data) : null,
			signal: this.controller.signal,
		})

		if (this.middlewares.length) {
			let result: Result | undefined

			for (const middleware of this.middlewares) {
				result = await middleware(response, result)
			}

			return result
		}

		return void 0
	}

	private toQueryString(obj: any): string {
		let paramsData = ''
		if (obj) {
			paramsData = Object.keys(obj)
				.map(k => `${k}=${encodeURIComponent(obj[k])}`)
				.join('&')
		}
		const params = '?' + paramsData

		if (this.preventCaching) {
			// Cache workaround. Only for GET requests - bodyMethods should not be cached.
			return params + (params.length > 0 ? '&' : '?') + '_=' + new Date().getTime()
		}

		return params
	}
}
