export default class HttpError extends Error {
	public statusCode: number
	public requestUrl: string
	public responseData: any

	constructor(statusCode: number, message: string, requestUrl: string, responseData?: any) {
		super(message)

		this.statusCode = statusCode
		this.requestUrl = requestUrl
		this.responseData = responseData
	}
}

export function isHttpError(error: Error): error is HttpError {
	return typeof (error as HttpError).statusCode !== 'undefined'
}
