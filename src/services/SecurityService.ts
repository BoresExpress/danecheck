export default class SecurityService {
	public static getRandomString(minLength: number, maxLength: number): string {
		if (minLength >= maxLength) {
			throw new Error('Invalid length pecified')
		}

		const minPrintChar = 32
		const maxPrintChar = 126
		const len = Math.random() * (maxLength - minLength) + minLength
		let retVal = ''
		for (let n = 0; n < len; n++) {
			retVal += String.fromCharCode(Math.random() * (maxPrintChar - minPrintChar) + minPrintChar)
		}

		return retVal
	}
}
