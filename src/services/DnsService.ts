import { ResourceTypeId, ResponseStatus } from '../api/GoogleHttpDns/GoogleHttpDnsResponse'
import GoogleHttpDnsApi from '../api/GoogleHttpDns/GoogleHttpDnsApi'

export default class DnsService {
	private readonly dnsApi: GoogleHttpDnsApi

	constructor() {
		this.dnsApi = new GoogleHttpDnsApi()
	}

	public async resolveSecure(host: string): Promise<string> {
		const response = await this.dnsApi.resolve(host, ResourceTypeId.A)
		console.log(response)

		if (!response) {
			throw new Error('Invalid response')
		}

		if (response.Status !== ResponseStatus.NOERROR) {
			if (response.Comment && response.Comment.startsWith('DNSSEC validation failure.')) {
				throw new Error('DNSSEC validation failed')
			}

			throw new Error('DNS server returned error')
		}

		if (!response.AD) {
			return ''
		}

		if (response.Answer.length === 0) {
			throw new Error('No responses')
		}

		return response.Answer[0].data
	}
}
