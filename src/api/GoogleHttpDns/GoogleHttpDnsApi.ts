import GoogleHttpDnsResponse, { ResourceTypeId } from './GoogleHttpDnsResponse'
import IHttpClient, { Returns } from '../../httpClient/IHttpClient'
import HttpClient from '../../httpClient/HttpClient'
import SecurityService from '../../services/SecurityService'
import errorHandlingMiddleware from '../../httpClient/middleware/errorHandlingMiddleware'
import jsonParsingMiddleware from '../../httpClient/middleware/jsonParsingMiddleware'

export default class GoogleHttpDnsApi {
	private static createHttpClient<Result>(): IHttpClient<Result> {
		const options = {
			host: 'https://dns.google',
			middlewares: [jsonParsingMiddleware<Result>(['application/x-javascript']), errorHandlingMiddleware],
		}
		return new HttpClient<Result>(options)
	}

	public resolve(hostName: string, type: ResourceTypeId): Returns<GoogleHttpDnsResponse> {
		const minLength = 4
		const maxLength = 10
		const data = {
			name: hostName,
			type,
			cd: false,
			ct: 'application/x-javascript',
			random_padding: SecurityService.getRandomString(minLength, maxLength),
		}

		return GoogleHttpDnsApi.createHttpClient<GoogleHttpDnsResponse>().get('/resolve', data)
	}
}
