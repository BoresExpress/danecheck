export enum ResponseStatus {
	NOERROR = 0, // DNS Query completed successfully
	FORMERR = 1, // DNS Query Format Error
	SERVFAIL = 2, // Server failed to complete the DNS request
	NXDOMAIN = 3, // Domain name does not exist
	NOTIMP = 4, // Function not implemented
	REFUSED = 5, // The server refused to answer for the query
	YXDOMAIN = 6, // Name that should not exist, does exist
	XRRSET = 7, // RRset that should not exist, does exist
	NOTAUTH = 8, // Server not authoritative for the zone
	NOTZONE = 9, // Name not in zone
}

export enum ResourceTypeId {
	A = 1,
	AAAA = 28,
	TLSA = 52,
}

export default interface GoogleHttpDnsResponse {
	Status: ResponseStatus // Standard DNS response code
	TC: boolean // Whether the response is truncated
	RD: boolean // Always true for Google Public DNS
	RA: boolean // Always true for Google Public DNS
	AD: boolean // Whether all response data was validated with DNSSEC
	CD: boolean // Whether the client asked to disable DNSSEC
	Question: {
		name: string // FQDN with trailing dot
		type: ResourceTypeId // SPF - Standard DNS RR type
	}[]
	Answer: {
		name: string // Always matches name in Question
		type: ResourceTypeId // SPF - Standard DNS RR type
		TTL: number // Record's time-to-live in seconds
		data: string // Data for SPF - quoted string
	}[]
	Comment: string
}
