import DnsService from '../services/DnsService'

chrome.runtime.onInstalled.addListener(detailes => {
	console.log('Installed!')
})

async function checkDnsSec(): Promise<boolean> {
	const dns = new DnsService()
	const failedServer = 'servfail.sidnlabs.nl'

	let ip = ''
	try {
		ip = await dns.resolveSecure(failedServer)
	} catch (ex) {
		console.log(ex)
		return false
	}

	return !ip
}

async function processStartup(): Promise<void> {
	const dnsSecStatus = await checkDnsSec()
	if (!dnsSecStatus) {
		console.log('DNSSEC check failed')
		chrome.browserAction.setIcon({ path: 'assets/icon/fail-dnssec-16.png' })
	}
}

chrome.runtime.onStartup.addListener(() => {
	processStartup()
})

async function processRequest(detailes: chrome.webRequest.WebResponseCacheDetails): Promise<void> {
	if (detailes.initiator) {
		return
	}

	const url = new URL(detailes.url)
	if (!url.protocol.startsWith('https')) {
		console.log('Resuest is not TLS secured')
		return
	}

	console.log(url.hostname)
	const dns = new DnsService()

	let ip = ''
	try {
		ip = await dns.resolveSecure(url.hostname)
	} catch (ex) {
		console.log('Failed to resolve hostname. Looks like DNSSEC failed.')
		console.log(ex)
		chrome.browserAction.setIcon({ path: 'assets/icon/fail-dnssec-16.png', tabId: detailes.tabId })
		return
	}

	if (!ip) {
		console.log('Domain is not secured with DNSSEC')
		chrome.browserAction.setIcon({ path: 'assets/icon/no-dnssec-16.png', tabId: detailes.tabId })
		return
	}

	if (ip !== detailes.ip) {
		console.log(`Resolved IP ${ip} does not match request ip ${detailes.ip}.`)
		chrome.browserAction.setIcon({ path: 'assets/icon/fail-dnssec-16.png', tabId: detailes.tabId })
		return
	}

	chrome.browserAction.setIcon({ path: 'assets/icon/ok-dnssec-16.png', tabId: detailes.tabId })

	const cert = await sslCertificate.get(url.hostname)
	console.log(cert)
}

chrome.webRequest.onCompleted.addListener(
	detailes => {
		processRequest(detailes) // just ignore async execution
	},
	{ urls: ['https://*/*'] },
)
